{
  description = "Behaviours exploration of the EP benchmark";

  nixConfig.bash-prompt = "[nix(expe-ep)] ";

  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/22.11;
    kapack.url = github:oar-team/nur-kapack;
  };

  outputs = { self, nixpkgs, kapack }: 
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
    kpkgs = kapack.packages.${system};
    pythonWithPackages = p: with p; [
      tomli
      requests
      kpkgs.execo
    ];
  in
  {
    devShells.${system}.default = pkgs.mkShell {
      packages = [
        (pkgs.python310.withPackages pythonWithPackages)
      ];
    };
  };
}
