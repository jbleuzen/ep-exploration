import re

# Running outside of G5K
# https://mimbert.gitlabpages.inria.fr/execo/execo_g5k.html#running-from-outside-grid5000
# Don't forget to replace USERNAME with your login

USERNAME = 'jbleuzen'

def host_rewrite_func(host):
    return re.sub("\.grid5000\.fr$", ".g5k", host)

def frontend_rewrite_func(host):
    return host + ".g5k"

g5k_configuration = {
    'oar_job_key_file': f'/home/{USERNAME}/.ssh/id_rsa',
    'default_frontend' : 'grenoble',
    'api_username' : f'{USERNAME}'
    }

default_connection_params = {'host_rewrite_func': host_rewrite_func}
default_frontend_connection_params = {'host_rewrite_func': frontend_rewrite_func}
