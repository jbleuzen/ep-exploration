import tomli
import time
from execo import *
from execo_g5k import *

class Expe:
    def __init__(self, conf):
        self.site = conf["site"]
        self.cluster = conf["cluster"]
        self.walltime = conf["walltime"]
        self.env = conf["env"]
        self.inputs = conf["inputs"]
        self.dest = conf["dest"]
        self.nb_nodes = 1

        self.connection_params = {'user':'root'}

        self.hosts = []
        self.jobs = []

    def reservations(self):
        submission = OarSubmission(
                f"nodes={self.nb_nodes}",
                self.walltime,
                "deploy",
                f"cluster={self.cluster}",
                )
        self.jobs = oarsub([(submission, self.site)])
        self.hosts = get_oar_job_nodes(self.jobs[0][0], self.site)
        to_deploy = kadeploy.Deployment(self.hosts, self.env)
        (deployed, undeployed) = kadeploy.deploy(to_deploy)
        self.hosts = list(deployed) 
        if len(undeployed) != 0:
            raise Exception("deployment incomplete")

    def send_data(self):
        Put(self.hosts, self.inputs, remote_location='/opt',
            connection_params=self.connection_params).run()
        #SshProcess("ls /opt", self.hosts, connection_params=self.connection_params).run()
        with Remote("ls /opt",
                    self.hosts, connection_params=self.connection_params) as command:
            command.run()
            command.wait()

    def preliminaries(self):
        with Remote("apt install xz-utils",
                    self.hosts, connection_params=self.connection_params) as command:
            command.run()
            command.wait()
        with Remote("cd /opt ; ./preliminaries.sh identification inputs/identification",
                    self.hosts, connection_params=self.connection_params) as command:
            command.run()
            command.wait()

    def retreive_data(self):
        Get(self.hosts, ["/tmp/experiments-data"], connection_params=self.connection_params,
            local_location=conf["dest"]).run()




if __name__ == "__main__":
    with open("ep-dahu.toml", mode="rb") as conf_file:
        conf = tomli.load(conf_file)

    expe = Expe(conf)
    expe.reservations()
    print("submission done")
    expe.send_data()
    print("inputs sended")
    time.sleep(2)
    expe.preliminaries()
    print("expe done")
    time.sleep(2)
    expe.retreive_data()
    print("file retreived")
